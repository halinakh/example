<?php

class HK_DistributionCenter_Adminhtml_Dc_DcController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('System'))->_title($this->__('Distribution Centers'));
        $this->loadLayout();
        $this->_setActiveMenu('cms');
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $dcCode = $this->getRequest()->getParam('dc_code');
        $dc = Mage::getModel('hk_distributioncenter/distributionCenter');

        if ($dcCode) {
            $dc->load($dcCode);
            if ($dc->getDcCode()) {
                Mage::register('current_dc', $dc);
            }
        }

        if (!$dc->getDcCode() && $dcCode) {
            $this->_getSession()->addError($this->__('This DC no longer exists.'));
            $this->_redirect('*/*/index');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);

            return $this;
        }

        $this->_title($this->__('Cms'))->_title($this->__('Distribution Centers'))->_title($this->__('Edit'));

        $this->loadLayout()
            ->_setActiveMenu('cms')
            ->_addBreadcrumb($this->__('Cms'), $this->__('Cms'))
            ->_addBreadcrumb($this->__('Distribution Centers'), $this->__('Distribution Centers'));

        return $this->renderLayout();
    }

    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $dcCode = $this->getRequest()->getParam('dc_code');

            if (!$this->getRequest()->getParam('is_new_dc')) {
                /** @var HK_DistributionCenter_Model_DistributionCenter $model */
                $model = Mage::getModel('hk_distributioncenter/distributionCenter')->load($dcCode);
                if (!$model->getDcCode()) {
                    $this->_getSession()->addError($this->__('This dc no longer exists.'));
                    return $this->_redirect('*/*/index');
                }
            } else {
                $model = Mage::getModel('hk_distributioncenter/distributionCenter');
                // check dccode in DB
                // should be 0
                $checkmodel = Mage::getModel('hk_distributioncenter/distributionCenter')
                    ->getCollection()
                    ->addFieldToFilter('dc_code', $dcCode);

                if(count($checkmodel) > 0){
                    $this->_getSession()->addError($this->__('DC code Must be Unique!'));
                    Mage::register('current_dc', $model);
                    return $this->_redirect('*/*/edit');
                };
            }

            $model->setData($data);

            // try to save it
            try {
                // save the data
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The DC has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('dc_code' => $model->getDcCode(), '_current'=>true));
                    return;
                }

                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException($e,
                    $this->__('An error occurred while saving the page.'));
            }

            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('dc_code' => $this->getRequest()->getParam('dc_code')));
            return;
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('dc_code')) {
            $title = "";
            try {
                // init model and delete
                $model = Mage::getModel('hk_distributioncenter/distributionCenter');
                $model->load($id);
                $title = $model->getTitle();
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Distribution center "%s" has been deleted.', $title));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find a DC to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/cms/distributioncenter/dc');
    }
}
